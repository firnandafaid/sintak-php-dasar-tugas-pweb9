<?php
$nama = "Muhammad Faid Firnanda Rahmatullah";
$umur = 19;
$pekerjaan = "Software Engineer";
$gaji = 11000000;

echo "===============Listing Program 9.1=================<br>";
echo "Nama: $nama<br>";
print("Umur: $umur <br>");
echo "Pekerjaan: $pekerjaan <br>";
print("Gaji Pribadi: $gaji <br>");
echo "================================================<br>";

if ($gaji > 1000000 && $gaji <= 2400000) {
    $pajak = 0.1;
    $thp = $gaji - ($gaji * $pajak);
    echo "Pajak Golongan 6: " . ($pajak * 100) . "%<br>";
    echo "Gaji Sebelum terkena pajak: Rp. $gaji <br>";
    echo "Gaji Setelah terkena pajak: Rp. $thp";

} elseif ($gaji > 2400000 && $gaji <=5000000) {
    $pajak = 0.1;
    $thp = $gaji - ($gaji * $pajak);
    echo "Pajak Golongan 5: " . ($pajak * 100) . "%<br>";
    echo "Gaji Sebelum terkena pajak: Rp. $gaji <br>";
    echo "Gaji Setelah terkena pajak: Rp. $thp";

} elseif ($gaji > 5000000 && $gaji <=10000000) {
    $pajak = 0.2;
    $thp = $gaji - ($gaji * $pajak);
    echo "Pajak Golongan 4: " . ($pajak * 100) . "%<br>";
    echo "Gaji Sebelum terkena pajak: Rp. $gaji <br>";
    echo "Gaji Setelah terkena pajak: Rp. $thp";

} elseif ($gaji > 10000000 && $gaji <=15000000) {
    $pajak = 0.3;
    $thp = $gaji - ($gaji * $pajak);
    echo "Pajak Golongan 3: " . ($pajak * 100) . "%<br>";
    echo "Gaji Sebelum terkena pajak: Rp. $gaji <br>";
    echo "Gaji Setelah terkena pajak: Rp. $thp";

} elseif ($gaji > 15000000 && $gaji <=20000000) {
    $pajak = 0.4;
    $thp = $gaji - ($gaji * $pajak);
    echo "Pajak Golongan 2: " . ($pajak * 100) . "%<br>";
    echo "Gaji Sebelum terkena pajak: Rp. $gaji <br>";
    echo "Gaji Setelah terkena pajak: Rp. $thp";

} elseif ($gaji > 20000000) {
    $pajak = 0.5;
    $thp = $gaji - ($gaji * $pajak);
    echo "Pajak Golongan 1: " . ($pajak * 100) . "%<br>";
    echo "Gaji Sebelum terkena pajak: Rp. $gaji <br>";
    echo "Gaji Setelah terkena pajak: Rp. $thp";

}else{
    echo "Gaji yang harus diinputkan Minimal Rp.1.000.000";
}
echo "<br>================================================<br>";
?>

<?php
$a = 5;
$b = 4;

$is_equal = ($a == $b) ? 1 : 0;
$is_not_equal = ($a != $b) ? 1 : 0;
$is_greater = ($a > $b) ? 1 : 0;
$is_less = ($a < $b) ? 1 : 0;
$is_not_equal_and_greater = (($a != $b) && ($a > $b)) ? 1 : 0;
$is_not_equal_or_greater = (($a != $b) || ($a > $b)) ? 1 : 0;

echo "<br>";
echo "<br>";
echo "<br>";
echo "==============Listing Program 9.2==================<br>";
echo "$a == $b : " . ($is_equal);
echo "<br>$a != $b : " . ($is_not_equal);
echo "<br>$a > $b : " . ($is_greater);
echo "<br>$a < $b : " . ($is_less);
echo "<br>($a == $b) && ($a > $b) : " . ($is_not_equal_and_greater);
echo "<br>($a == $b) || ($a > $b) : " . ($is_not_equal_or_greater);
echo "<br>!($a == $b) : " . ((!($a == $b)) ? 1 : 0);  
echo "<br>($a >= $b) : " . (($a >= $b) ? 1 : 0);    
echo "<br>($a <= $b) : " . (($a <= $b) ? 1 : 0);   
echo "<br>==============================================<br>";  
?>